﻿using System.Web;
using System.Web.Optimization;

namespace BookStore
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/scripts/jquery").Include(
                        "~/Content/scripts/jquery-{version}.js"));


            bundles.Add(new ScriptBundle("~/Content/scripts/bootstrap").Include(
                      "~/Content/scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.css",
                      "~/Content/css/styles.css"));
        }
    }
}
