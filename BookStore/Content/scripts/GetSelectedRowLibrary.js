﻿// Declaración de clase
class GetSelectedRowLibrary {
    // Constructor
    constructor() { }
    // Método que inicializa la selección de filas en la tabla con id: 'tableBody'
    rows(seeControl, editControl, deleteControl, tableBody, cssClass) {
        const doc = document,
            table = doc.getElementById(`${tableBody}`);

        if (table !== null) {
            let tableChildrens = [...table.children];

            tableChildrens.forEach(row => {
                row.addEventListener('click', e => {
                    e.stopImmediatePropagation();
                    let rowSelected = e.target;
                    if (rowSelected.parentElement.classList.contains(`${cssClass}`)) {
                        this.clearRows(tableBody, cssClass);
                        this.disableControls(seeControl, editControl, deleteControl);
                    } else {
                        this.clearRows(tableBody, cssClass);
                        rowSelected.parentElement.classList.add(`${cssClass}`);
                        this.enableControls(seeControl, editControl, deleteControl);
                    }
                });
            });
        } 
    }
    // Método que limpia las filas seleccionadas
    clearRows(tableBody, cssClass) {
        const doc = document,
            table = doc.getElementById(`${tableBody}`),
            tableChildrens = [...table.children];

        tableChildrens.forEach(row => {
            row.classList.remove(`${cssClass}`);
        });
    }
    // Retorna el id de la fila seleccionada
    getSelectedRow(table, cssClass) {
        const doc = document,
            tableBody = doc.getElementById(`${table}`);

        if (tableBody !== null) {
            const tableBodyChildren = [...tableBody.children];
            let selectedRow;

            selectedRow = tableBodyChildren.filter(rowSelected => {
                return rowSelected.classList.contains(`${cssClass}`);
            });

            if (selectedRow.length == 0) {
                return false;
            } else {
                return selectedRow[0].firstElementChild.innerText;;
            }
        }
    }
    // Método que deshabilita los controles si no hay registro seleccionado
    disableControls(seeControl, editControl, deleteControl) {
        const doc = document;
        doc.getElementById(`${seeControl}`).classList.add('disabled');
        doc.getElementById(`${editControl}`).classList.add('disabled');
        doc.getElementById(`${deleteControl}`).classList.add('disabled');
    }
    // Método para habilitar los controles si hay un registro seleccionado
    enableControls(seeControl, editControl, deleteControl) {
        const doc = document;
        doc.getElementById(`${seeControl}`).classList.remove('disabled');
        doc.getElementById(`${editControl}`).classList.remove('disabled');
        doc.getElementById(`${deleteControl}`).classList.remove('disabled');
    }
}