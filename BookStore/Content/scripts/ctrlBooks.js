﻿// Iniciando librerías
const row = new GetSelectedRowLibrary();

function ctrlBooks() {
    const doc = document;
    row.rows('seeBook', 'editBook', 'deleteBook', 'tableBody', 'row-selected');
    doc.getElementById('seeBook').addEventListener('click', seeBook);
    doc.getElementById('editBook').addEventListener('click', editBook);
    doc.getElementById('deleteBook').addEventListener('click', deleteBook);
    userFeedback();
}

function seeBook() {
    let rowSelected = row.getSelectedRow('tableBody','row-selected');

    if (rowSelected == false) {
        alert('Por favor, seleccione un registro de la lista!');
    } else {
        this.setAttribute('href', `libro/buscar/${rowSelected}`);
    }
}

function editBook() {
    let rowSelected = row.getSelectedRow('tableBody', 'row-selected');

    if (rowSelected == false) {
        alert('Por favor, seleccione un registro de la lista!');
    } else {
        this.setAttribute('href', `libro/editar/${rowSelected}`);
    }
}

function deleteBook() {
    const doc = document,
        modal = doc.getElementById('modalWindow');

        modal.classList.add('active');
        doc.getElementById('btnCloseModal').addEventListener('click', closeModal);
        doc.getElementById('btnModalCancelar').addEventListener('click', closeModal);
        doc.getElementById('btnModalAceptar').addEventListener('click', deleteRegisterSelected);
}

function closeModal() {
    const doc = document,
        modal = doc.getElementById('modalWindow');

    modal.classList.remove('active');
}

function deleteRegisterSelected() {
    let rowSelected = row.getSelectedRow('tableBody', 'row-selected');

    if (rowSelected == false) {
        alert('Por favor, seleccione un registro de la lista!');
    } else {
        location.href = `libro/eliminar/${rowSelected}`;
    }
}

function userFeedback() {
    const doc = document,
        feedback = doc.getElementById('sucessMessage');

    if (feedback != null) {
        feedback.classList.add('active');
        removeFeedback();
    }
}

function removeFeedback() {
    const doc = document,
        feedback = doc.getElementById('sucessMessage');

    setTimeout(() => {
        feedback.classList.remove('active');
        feedback.textContent = '';
    }, 3500);
}

ctrlBooks();
