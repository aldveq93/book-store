﻿$(document).ready(function () {
    $('#bookTable').DataTable({
        language: {
            "search": "Buscar",
            "info": "Mostrando _START_ a _END_ entradas de _TOTAL_ registros",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "infoFiltered": " - filtrado de _MAX_ entradas en total",
            "lengthMenu": "Mostrar _MENU_ entradas"
        },
        "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "Todos"]]
    });
    $('[data-toggle="tooltip"]').tooltip();
});