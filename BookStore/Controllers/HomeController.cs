﻿using BookStore.Models;
using BookStore.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class HomeController : Controller
    {
        BookStoreContext _bookContext;

        public HomeController() {
            _bookContext = new BookStoreContext();
        }

        public ActionResult Index()
        {
            var books =
                (from b in _bookContext.books
                 select new BookIndexViewModel()
                 {
                     bookId = b.bookId,
                     bookName = b.bookName,
                     bookDescription = b.bookDescription,
                     bookImagePath = b.bookImagePath
                 }).ToList();

            return View(books);
        }
    }
}