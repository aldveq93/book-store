﻿using BookStore.businessLogic;
using BookStore.Models;
using BookStore.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class libroController : Controller
    {
        private BookBusinessLogic bookBusinessLogic = new BookBusinessLogic();
        private BookStoreContext _bookContext;

        public libroController() {
            _bookContext = new BookStoreContext();
        }

        // GET: book/list
        public ActionResult index()
        {
            var books = bookBusinessLogic.GetBooks();
            return View(books);
        }

        // GET: book/insert
        [HttpGet]
        public ActionResult nuevo()
        {
            var viewModel = new BookFormViewModel
            {
                book = new Book(),
                categories = _bookContext.categories.ToList(),
                clasifications = _bookContext.clasification.ToList()
            };

            return View("libroForm", viewModel);
        }

        // POST: book/save
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult guardar(Book book)
        {
            if (!ModelState.IsValid) {

                var viewModel = new BookFormViewModel
                {
                    book = book,
                    categories = _bookContext.categories.ToList(),
                    clasifications = _bookContext.clasification.ToList()
                };

                return View("libroForm", viewModel);
            }

            if (book.bookId == 0)
            {
                bookBusinessLogic.SaveBook(book);
                TempData["SucessMessage"] = "¡Libro insertado con éxito!";
            }
            else {
                bookBusinessLogic.EditBook(book);
                TempData["SucessMessage"] = "¡Libro editado con éxito!";
            }

            return RedirectToAction("index","libro");
        }

        // GET: book/search/id
        [HttpGet]
        public ActionResult buscar(int? id)
        {
            if (id.HasValue)
            {
                var book = bookBusinessLogic.GetBookById(id);

                if (book == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    var searchBook = new BookFormViewModel
                    {
                        book = book,
                        categories = _bookContext.categories.ToList(),
                        clasifications = _bookContext.clasification.ToList()
                    };
                    return View(searchBook);
                }
            }
            else {
                return new HttpStatusCodeResult(500);
            }
        }

        //GET: book/edit/id
       [HttpGet]
        public ActionResult editar(int? id)
        {
            if (id.HasValue)
            {
                var book = bookBusinessLogic.GetBookById(id);

                if (book == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    var viewModel = new BookFormViewModel
                    {
                        book = book,
                        categories = _bookContext.categories.ToList(),
                        clasifications = _bookContext.clasification.ToList()
                    };

                    return View("libroForm", viewModel);
                }
            }
            else {
                return new HttpStatusCodeResult(500);
            }
            
        }

        // DELETE: book/delete/id
        public ActionResult eliminar(int? id) {
            if (id.HasValue)
            {
                var book = bookBusinessLogic.DeleteBook(id);

                if (book == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    TempData["SucessMessage"] = "¡Libro eliminado con éxito!";
                    return RedirectToAction("index","libro");
                }
            }
            else {
                return new HttpStatusCodeResult(500);
            }
        }
    }
}