namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialBookModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        bookId = c.Int(nullable: false, identity: true),
                        bookName = c.String(nullable: false, maxLength: 250),
                        bookCategory = c.String(nullable: false, maxLength: 250),
                        bookAuthor = c.String(nullable: false, maxLength: 250),
                        bookClasificationPermitted = c.String(nullable: false, maxLength: 250),
                        bookYear = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.bookId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Books");
        }
    }
}
