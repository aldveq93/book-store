namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateBooks : DbMigration
    {
        public override void Up()
        {
            Sql("insert into books (bookName, bookCategory, bookAuthor, bookClasificationPermitted, bookYear) values ('The Inspector Sherlock Holmes','Adventures','Mike Wasasky','PG-13',1999)");
            Sql("insert into books (bookName, bookCategory, bookAuthor, bookClasificationPermitted, bookYear) values ('The Rayo Mcqueen','Adventures','Mike Wasasky','PG-13',1995)");
            Sql("insert into books (bookName, bookCategory, bookAuthor, bookClasificationPermitted, bookYear) values ('The Coward Roberts Wallen','Action','Pete Rivers','R',1998)");
            Sql("insert into books (bookName, bookCategory, bookAuthor, bookClasificationPermitted, bookYear) values ('The Adventures of Tom Sawyer','Adventures','Tom Sawyer','PG-13',2000)");
        }
        
        public override void Down()
        {
        }
    }
}
