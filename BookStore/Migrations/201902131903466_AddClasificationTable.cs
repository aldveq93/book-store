namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClasificationTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clasifications",
                c => new
                    {
                        clasificationId = c.Int(nullable: false, identity: true),
                        clasificationName = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.clasificationId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Clasifications");
        }
    }
}
