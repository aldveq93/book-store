namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateCategories : DbMigration
    {
        public override void Up()
        {
            Sql("insert into Categories (categoryName) values ('Aventuras')");
            Sql("insert into Categories (categoryName) values ('Acci�n')");
            Sql("insert into Categories (categoryName) values ('Romance')");
            Sql("insert into Categories (categoryName) values ('Terror')");
        }
        
        public override void Down()
        {
        }
    }
}
