namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PupulateClasification : DbMigration
    {
        public override void Up()
        {
            Sql("insert into Clasifications (clasificationName) values ('PG-13')");
            Sql("insert into Clasifications (clasificationName) values ('PG-18')");
            Sql("insert into Clasifications (clasificationName) values ('R')");
        }
        
        public override void Down()
        {
        }
    }
}
