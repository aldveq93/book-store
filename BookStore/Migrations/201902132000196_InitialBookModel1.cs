namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialBookModel1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "bookCategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.Books", "bookClasificationId", c => c.Int(nullable: false));
            AddColumn("dbo.Books", "bookCategory_categoryId", c => c.Int());
            AddColumn("dbo.Books", "bookClasificationPermitted_clasificationId", c => c.Int());
            CreateIndex("dbo.Books", "bookCategory_categoryId");
            CreateIndex("dbo.Books", "bookClasificationPermitted_clasificationId");
            AddForeignKey("dbo.Books", "bookCategory_categoryId", "dbo.Categories", "categoryId");
            AddForeignKey("dbo.Books", "bookClasificationPermitted_clasificationId", "dbo.Clasifications", "clasificationId");
            DropColumn("dbo.Books", "bookCategory");
            DropColumn("dbo.Books", "bookClasificationPermitted");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Books", "bookClasificationPermitted", c => c.String(nullable: false, maxLength: 250));
            AddColumn("dbo.Books", "bookCategory", c => c.String(nullable: false, maxLength: 250));
            DropForeignKey("dbo.Books", "bookClasificationPermitted_clasificationId", "dbo.Clasifications");
            DropForeignKey("dbo.Books", "bookCategory_categoryId", "dbo.Categories");
            DropIndex("dbo.Books", new[] { "bookClasificationPermitted_clasificationId" });
            DropIndex("dbo.Books", new[] { "bookCategory_categoryId" });
            DropColumn("dbo.Books", "bookClasificationPermitted_clasificationId");
            DropColumn("dbo.Books", "bookCategory_categoryId");
            DropColumn("dbo.Books", "bookClasificationId");
            DropColumn("dbo.Books", "bookCategoryId");
        }
    }
}
