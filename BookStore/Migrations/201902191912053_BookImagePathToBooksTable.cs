namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BookImagePathToBooksTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "bookImagePath", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "bookImagePath");
        }
    }
}
