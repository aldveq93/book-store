namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReStructureOfBookImagePath : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Books", "bookImagePath", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Books", "bookImagePath", c => c.String(nullable: false));
        }
    }
}
