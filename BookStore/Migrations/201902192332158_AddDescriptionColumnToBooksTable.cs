namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDescriptionColumnToBooksTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "bookDescription", c => c.String(nullable: false, maxLength: 640));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "bookDescription");
        }
    }
}
