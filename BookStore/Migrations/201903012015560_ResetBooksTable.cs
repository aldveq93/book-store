namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ResetBooksTable : DbMigration
    {
        public override void Up()
        {
            Sql("truncate table Books");
        }
        
        public override void Down()
        {
        }
    }
}
