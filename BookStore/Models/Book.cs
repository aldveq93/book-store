﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BookStore.Models
{
    public class Book
    {
        // Atributos
        [Key]
        public int bookId { set; get; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El nombre del libro es requerido.")]
        [StringLength(250)]
        public string bookName { set; get; }

        public Category bookCategory { set; get; }

        [Required(ErrorMessage = "La categoría del libro es requerida.")]
        [Display(Name = "Categoría")]
        public int bookCategoryId { set; get; }

        [Display(Name = "Autor")]
        [Required(ErrorMessage = "El autor del libro es requerido.")]
        [StringLength(250)]
        public string bookAuthor { set; get; }

        public Clasification bookClasificationPermitted { set; get; }
        
        [Required(ErrorMessage = "La clasificación del libro es requerida.")]
        [Display(Name = "Clasificación")]
        public int bookClasificationId { set; get; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#}")]
        [Required(ErrorMessage = "El año del libro es requerido.")]
        [Display(Name = "Año")]
        [Range(1900,2018,ErrorMessage ="El año debe ser un número entero entre 1900 al 2018")]
        public int bookYear { set; get; }

        [Required(ErrorMessage ="La descripción del libro es requerida.")]
        [StringLength(640)]
        [Display(Name ="Descripción")]
        public string bookDescription { set; get; }

        [StringLength(250)]
        public string bookImagePath { set; get; }

        [NotMapped]
        [Display(Name = "Portada del libro")]
        public HttpPostedFileBase bookImageFile { set; get; }
    }
}