﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookStore.Models
{
    public class BookStoreContext : DbContext
    {
        public DbSet<Book> books { set; get; }
        public DbSet<Category> categories { set; get; }
        public DbSet<Clasification> clasification { set; get; }

        public BookStoreContext()
            : base("BookStoreContext")
        { }
    }
}