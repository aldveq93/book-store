﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookStore.Models
{
    public class Category
    {
        [Key]
        public int categoryId { set; get; }

        [Required]
        [StringLength(250)]
        public string categoryName { set; get; }
    }
}