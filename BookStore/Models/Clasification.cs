﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookStore.Models
{
    public class Clasification
    {
        [Key]
        public int clasificationId { set; get; }

        [Required]
        [StringLength(250)]
        public string clasificationName { set; get; }
    }
}