﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace BookStore.Utilidades
{
    public class Utilidades
    {
        private const string defaultImagePathInDB = "book-without-image.png";

        public static Book uploadBookImage(Book book) {

            string fileName = Path.GetFileNameWithoutExtension(book.bookImageFile.FileName);
            string extension = Path.GetExtension(book.bookImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("ddmmyyyyss") + extension;
            book.bookImagePath = "~/../../Content/images/booksImages/" + fileName;
            fileName = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/images/booksImages"), fileName);
            book.bookImageFile.SaveAs(fileName);

            return book;
        }

        public static void deleteUploadedBookImage(Book book) {

            string addNewImagePath = "~" + cutBookImagePath(book.bookImagePath);
            string imagePathFromDB = HostingEnvironment.MapPath(addNewImagePath);

            bool boolRestult = ValidateDefaultImage(imagePathFromDB);

            if (boolRestult == false)
            {
                if (File.Exists(imagePathFromDB))
                {
                    try
                    {
                        File.Delete(imagePathFromDB);
                    }
                    catch (IOException ex)
                    {
                        Console.WriteLine("No se ha podido eliminar la imagen: " + ex.Message);
                    }
                }
            }
        }

        public static string cutBookImagePath(string imagePath) {
            string newImagePath;    
            newImagePath = imagePath.Remove(0, 7);
            return newImagePath;
        }

        private static bool ValidateDefaultImage(string imagePath) {
            if (imagePath.Contains(defaultImagePathInDB))
            {
                return true;
            }
            else {
                return false;
            }
        }
    }
}