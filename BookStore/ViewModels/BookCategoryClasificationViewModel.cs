﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.ViewModels
{
    public class BookCategoryClasificationViewModel
    {
        public int bookId { set; get; }
        public string bookName { set; get; }
        public string bookCategory { set; get; }
        public string bookAuthor { set; get; }
        public string bookClasification { set; get; }
        public int bookYear { set; get; }
    }
}