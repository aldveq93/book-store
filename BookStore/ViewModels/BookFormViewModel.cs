﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.ViewModels
{
    public class BookFormViewModel
    {
        public IEnumerable<Category> categories { set; get; }
        public IEnumerable<Clasification> clasifications { set; get; }
        public Book book { set; get; }
        public string formTitle { get { return book.bookId != 0 ? "Editar libro" : "Nuevo libro"; } }
    }
}