﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.ViewModels
{
    public class BookIndexViewModel
    {
        public int bookId { set; get; }
        public string bookName { set; get; }
        public string bookDescription { set; get; }
        public string bookImagePath { set; get; }
    }
}