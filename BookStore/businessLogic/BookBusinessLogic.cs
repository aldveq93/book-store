﻿using BookStore.Models;
using BookStore.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.businessLogic
{
    public class BookBusinessLogic
    {
        private BookStoreContext _bookContext;
        private const string defaultImageBook = "~/../../Content/images/booksImages/book-without-image.png";

        public BookBusinessLogic() {
            _bookContext = new BookStoreContext();
        }

        // Método que devuelve la lista de libros para mostrar en pantalla
        public List<BookCategoryClasificationViewModel> GetBooks() {

            var books =
                (from b in _bookContext.books
                 join c in _bookContext.categories on b.bookCategoryId equals c.categoryId
                 join cl in _bookContext.clasification on b.bookClasificationId equals cl.clasificationId
                 select new BookCategoryClasificationViewModel()
                 {
                     bookId = b.bookId,
                     bookName = b.bookName,
                     bookCategory = c.categoryName,
                     bookAuthor = b.bookAuthor,
                     bookClasification = cl.clasificationName,
                     bookYear = b.bookYear
                 }).ToList();

            return books;
        }

        // Método que devuelve un libro por su id
        public Book GetBookById(int? id) {
            Book book = null;

            if (id.HasValue)
            {
                book = _bookContext.books.SingleOrDefault(b => b.bookId == id);
                if (book == null)
                {
                    return book;
                }
                else {
                    book.bookImagePath = Utilidades.Utilidades.cutBookImagePath(book.bookImagePath);
                }
            }
            else {
                return book;
            }

            return book;
        }

        // Método para eliminar un libro
        public Book DeleteBook(int? id) {

            var book = _bookContext.books.SingleOrDefault(b => b.bookId == id);

            if (book != null) {
                Utilidades.Utilidades.deleteUploadedBookImage(book);
                _bookContext.books.Remove(book);
                _bookContext.SaveChanges();
            }

            return book;
        }

        // Método para guardar un libro con una imagen seleccionada
        public void SaveBook(Book book) {

            if (book.bookImageFile != null)
            {
                Book bookReStructured = Utilidades.Utilidades.uploadBookImage(book);
                _bookContext.books.Add(bookReStructured);
            }
            else {
                book.bookImagePath = defaultImageBook;
                _bookContext.books.Add(book);
            }

            _bookContext.SaveChanges();
        }

        // Método para editar un libro
        public void EditBook(Book book) {
            // Búsqueda del libro por id en la base de datos
            var bookInDb = _bookContext.books.SingleOrDefault(b => b.bookId == book.bookId);
            // Código que se ejecuta si viene una imagen
            if (book.bookImageFile != null)
            {
                Utilidades.Utilidades.deleteUploadedBookImage(bookInDb);
                Book bookWithNewImagePath = Utilidades.Utilidades.uploadBookImage(book);

                bookInDb.bookName = bookWithNewImagePath.bookName;
                bookInDb.bookAuthor = bookWithNewImagePath.bookAuthor;
                bookInDb.bookCategoryId = bookWithNewImagePath.bookCategoryId;
                bookInDb.bookClasificationId = bookWithNewImagePath.bookClasificationId;
                bookInDb.bookYear = bookWithNewImagePath.bookYear;
                bookInDb.bookImagePath = bookWithNewImagePath.bookImagePath;
                bookInDb.bookDescription = bookWithNewImagePath.bookDescription;
            }
            else { //Código que se ejecuta si no se seleccionó una imagen al editar un libro
                bookInDb.bookName = book.bookName;
                bookInDb.bookAuthor = book.bookAuthor;
                bookInDb.bookCategoryId = book.bookCategoryId;
                bookInDb.bookClasificationId = book.bookClasificationId;
                bookInDb.bookYear = book.bookYear;
                bookInDb.bookDescription = book.bookDescription;
            }

            _bookContext.SaveChanges();
        }

    }
}